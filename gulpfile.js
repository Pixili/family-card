var gulp = require('gulp');

var activetheme = "tagcity";
var themepath = './web/app/themes/' + activetheme;
var buildpath = themepath + '/build';
var lessFiles = themepath + '/bootstrap/less/style.less';

var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var runTimestamp = Math.round(Date.now() / 1000);

gulp.task('iconfont', function() {
  return gulp.src(['assets/icons/*.svg'])
    .pipe(iconfontCss({
      fontName: 'icons',
      path: 'assets/fonts/templates/_icons.less',
      targetPath: '/../../assets/icons/icons.less',
      fontPath: ''
    }))
    .pipe(iconfont({
      fontName: 'icons', // required
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff', 'woff2'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
    }))
    .on('glyphs', function(glyphs, options) {
      // CSS templating, e.g.
      console.log(glyphs, options);
    })
    .pipe(gulp.dest('assets/fonts/icons/'));
});

var fs = require('fs');
var jade = require('react-jade');
var argv = require('yargs').argv;

gulp.task('react-jade', function() {

    console.log(argv.input);

      fs.writeFileSync(argv.output, 'var template = ' + jade.compileFile(argv.input));
});


var replace = require('gulp-replace');

gulp.task('templates', function(){

  var o = argv.output;
  var file = o.substring(0, o.lastIndexOf("/"));;

  console.log(file);

  gulp.src(argv.output)
    .pipe(replace(/"#{([a-z0-9._]+)}"/g, '$1'))
    .pipe(replace(/"#{([a-z0-9._]+)} "/g, '$1'))
    .pipe(replace(/"(self.*)"/g, '$1'))
    .pipe(replace(/value:/g, 'defaultValue:'))
    .pipe(replace(/selected:/g, 'value:'))
    .pipe(replace(/= locals/g, '= self.state'))
    .pipe(replace(/locals/g, 'self'))
    .pipe(replace(/#{([a-z0-9._]+)}/g, "'+ $1 +'"))
    .pipe(replace(/\+'"/g, ''))
    .pipe(replace(/\+' "/g, ''))
    .pipe(replace(/"'\+/g, ''))
    .pipe(gulp.dest(file));
});


gulp.task('jade', ['react-jade', 'templates']);
