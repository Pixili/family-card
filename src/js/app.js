import '../css/app.less';
import $ from 'jquery';
import LazyLoad from 'vanilla-lazyload';
import slick from 'slick-carousel';
import 'bootstrap/js/dropdown';
import 'bootstrap/js/collapse';

var invisible = new LazyLoad({
  elements_selector: ".lazy-invisible",
  data_src: "bg",
  skip_invisible: false
});

var lazy = new LazyLoad({
  elements_selector: ".lazy",
  data_src: "bg",
  skip_invisible: true
});

var lazy = new LazyLoad({
  elements_selector: "img",
  data_src: "src",
  skip_invisible: true
});


$('.responsive-logocontainer').slick({
  dots: false,
  infinite: true,
  arrows: true,
  speed: 300,
  slidesToShow: 10,
  slidesToScroll: 40,
  responsive: [
    {
      breakpoint: 3000,
      settings: {
        slidesToShow: 14,
        slidesToScroll: 14,
        infinite: true,
        dots: false,
        arrows: true,
      }
    },
    {
      breakpoint: 1600,
      settings: {
        slidesToShow: 10,
        slidesToScroll: 10,
        infinite: true,
        dots: false,
        arrows: true,
      }
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 6,
        infinite: true,
        dots: false,
        arrows: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: true,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
