var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require("path");

var isProd = process.env.NODE_ENV === 'production';
var cssDev = [{
    loader: "style-loader"
}, {
    loader: "css-loader"
}, {
    loader: "less-loader", options: {
        strictMath: true,
        noIeCompat: true
    }
}];
var cssProd = ExtractTextPlugin.extract({
    fallback: "style-loader",
    use: [
    {
        loader: "css-loader"
    }, {
        loader: "less-loader",
    }]
});

var cssConfig = isProd ? cssProd : cssDev;

module.exports = {
    entry: {
        app: './src/js/app.js',
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: '[name].bundle.js'
    },
    module: {
            rules: [
            {
                test: /\.css$/, 
                use: cssConfig
            },
            {
                test: /\.jade$/,
                loader: 'pug-loader',
            },
            {
                test: /\.less$/,
                use: cssConfig
            },
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.jsx$/, exclude: /node_modules/, loader: "babel-loader" },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                  name: 'fonts/[name].[ext]',
                },
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use:  [
                    'image-webpack-loader',                    
                    'file-loader?name=img/[name].[ext]',
                ],
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000,
        hot: true,
        stats: "errors-only",
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Familily Card',
            minify: {
                collapseWhitespace: true,
            },
            excludeChunks: ['contact'],
            hash: true,
            filename: 'index.html',
            template: 'src/index.jade',
        }),
        new HtmlWebpackPlugin({
            title: 'TagCitico',
            minify: {
                collapseWhitespace: true,
            },
            excludeChunks: ['contact'],
            hash: true,
            filename: 'webshop-az.html',
            template: 'src/webshop-az.jade',
        }),
        new HtmlWebpackPlugin({
            title: 'TagCitico',
            minify: {
                collapseWhitespace: true,
            },
            hash: true,
            filename: 'detail.html',
            template: 'src/detail.jade',
        }),
        /*new HtmlWebpackPlugin({
            title: 'Contact',
            minify: {
                collapseWhitespace: true,
            },
            chunks: ['contact'],
            hash: true,
            template: './src/contact.pug', 
            filename: 'contact.html'           
        }),*/
        new ExtractTextPlugin({
            filename: 'style.css',
            disable: !isProd, 
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Util: "exports-loader?Util!bootstrap/js/dist/util",
            Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
        }),
        new webpack.HotModuleReplacementPlugin(),
    ]
} 